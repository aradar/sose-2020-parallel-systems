# General

This is our project (Nora Kießling, Ralph Schlett) for the parallel systems 
class (summer semester 2020). The goal of the project is to build a small n 
body simulator with a sequential as well as two parallel implementations of
the brute force algorithm. It also contains a rather basic OpenGL visualizer
of the current status of the simulation.

# Building the project
The project uses cmake as its build environment and is therefor rather 
straightforward to compile. To compile it you first have to configure it like
for example like this:

- make a build dir `mkdir example-build`
- cd into the build dir `cd example-build`
- run the configuration `cmake -DCMAKE_INSTALL_PREFIX=install -DCMAKE_BUILD_TYPE=Release ../`

After configuring the project you can build it. This will also fetch the 
dependencies and build them.

- run the build command `cmake --build . --target nbs_app nbs_tests nbs_docs -- -j$(nproc --all)`

- optionally it is possible to install the newly build binaries into a clean
  dir structure with the following command 
  `cmake --install .` (this must be run from inside the build dir)

# Usage
```
Usage:
  nbs [-?|-h|--help] [-r|--use-random-body-gen] [-n|--num-random-bodies] [-b|--bodies-json-file] [--benchmark-mode] [--num-benchmark-steps] [--simulation-backend] [--allow-collisions] [--collision-distance] [--sim-time-step] [--vis-num-steps-per-frame] [--rng-seed] [--min-rng-mass] [--max-rng-mass] [--min-rng-pos] [--max-rng-pos] [--min-rng-vel] [--max-rng-vel] [--opencl-platform-id] [--opencl-device-id] [--print-opencl-platforms] [--print-opencl-devices]

Options, arguments:
  -?, -h, --help

  Display usage information.

  -r, --use-random-body-gen

  Enables random body generation. If this is not set -b/--bodies-json-file must be supplied

  -n, --num-random-bodies <num-random-bodies>

  Defines how many random bodies get generated.

  -b, --bodies-json-file <bodies-json-file>

  A JSON input file which contains the bodies for the simulation.

  --benchmark-mode

  Enables the benchmark mode. If enabled the visualization will be disabled and --num-benchmark-steps will be simulated and the time the simulation took will be printed in seconds.

  --num-benchmark-steps <num-benchmark-steps>

  Defines the number of steps the benchmark mode calculates before exiting.

  --simulation-backend <simulation-backend>

  Defines the Simulator class which gets used for the simulation.

  --allow-collisions

  If activated bodies to collide and merge if they are closer than --collision-distance.

  --collision-distance <collision-distance>

  Sets the distance at which bodies get merged if --allow-collisions is activated.

  --sim-time-step <sim-time-step>

  Defines the used time step for the simulation.

  --vis-num-steps-per-frame <vis-num-steps-per-frame>

  Defines how many steps a simulated between drawn frames.

  --rng-seed <rng-seed>

  Defines the seed used during random body generation. If the string is empty it will just use non deterministic random number generator if possible.

  --min-rng-mass <min-rng-mass>

  Defines the minimum value during random body mass generation

  --max-rng-mass <max-rng-mass>

  Defines the maximum value during random body mass generation

  --min-rng-pos <min-rng-pos>

  Defines the minimum value during random body position generation

  --max-rng-pos <max-rng-pos>

  Defines the maximum value during random body position generation

  --min-rng-vel <min-rng-vel>

  Defines the minimum value during random body velocity generation

  --max-rng-vel <max-rng-vel>

  Defines the maximum value during random body velocity generation

  --opencl-platform-id <opencl-platform-id>

  Defines the OpenCL platform used by the OpenCL backend.

  --opencl-device-id <opencl-device-id>

  Defines the OpenCL device used by the OpenCL backend.

  --print-opencl-platforms

  Prints available OpenCL platforms and exits.

  --print-opencl-devices

  Prints available OpenCL devices and exits.

```
