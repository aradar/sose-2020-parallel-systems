#!/usr/bin/env bash

export CC=clang
export CXX=clang++
export CXXFLAGS="-fno-omit-frame-pointer -fsanitize=thread"

SCRIPT_PATH="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
PROJECT_ROOT=$(realpath $SCRIPT_PATH/..)

REL_BUILD_DIR=tsan-tests-build
BUILD_DIR=$PROJECT_ROOT/$REL_BUILD_DIR

cd $PROJECT_ROOT
echo "--------------------------------------------------------------------------------"
echo "building project"
echo "--------------------------------------------------------------------------------"

if [ ! -d "$BUILD_DIR" ]
then
    mkdir -p $BUILD_DIR
    cd $BUILD_DIR
    cmake \
        -DCMAKE_BUILD_TYPE=Debug \
        $PROJECT_ROOT
    if [ $? -ne 0 ]; then
        echo ""
        echo "project configuration failed! see the error above for more information."
        exit $?
    fi
    cmake --build . --target nbs_tests -- -j$(nproc --all)
    if [ $? -ne 0 ]; then
        echo ""
        echo "project building failed! see the error above for more information."
        exit $?
    fi
else
    echo "found old build. reusing it (if this is not desired delete $BUILD_DIR and $INSTALL_DIR"
fi

echo "--------------------------------------------------------------------------------"
echo "running tests with thread sanitizer enabled during compiling"
echo "--------------------------------------------------------------------------------"

cd $PROJECT_ROOT
TSAN_OPTIONS="ignore_noninstrumented_modules=1" ./$REL_BUILD_DIR/tests/nbs-test
