#!/usr/bin/env bash

BENCHMARK_STEPS=1
NUM_REPEATS=5

SCRIPT_PATH="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
PROJECT_ROOT=$(realpath $SCRIPT_PATH/..)

BUILD_DIR=$PROJECT_ROOT/benchmark-build
INSTALL_DIR=$PROJECT_ROOT/benchmark-install

cd $PROJECT_ROOT

echo "--------------------------------------------------------------------------------"
echo "building project"
echo "--------------------------------------------------------------------------------"

if [ ! -d "$BUILD_DIR" ] 
then
    mkdir -p $BUILD_DIR
    mkdir -p $INSTALL_DIR
    cd $BUILD_DIR
    cmake \
        -DCMAKE_BUILD_TYPE=Release \
        -DCMAKE_INSTALL_PREFIX=$INSTALL_DIR \
        $PROJECT_ROOT
    if [ $? -ne 0 ]; then
        echo ""
        echo "project building failed! see the error above for more information."
        exit $?
    fi
    cmake --build . --target nbs_app nbs_docs nbs_tests -- -j$(nproc --all)
    if [ $? -ne 0 ]; then
        echo ""
        echo "project building failed! see the error above for more information."
        exit $?
    fi
    cmake --install .
    if [ $? -ne 0 ]; then
        echo ""
        echo "project building failed! see the error above for more information."
        exit $?
    fi
else
    echo "found old build. reusing it (if this is not desired delete $BUILD_DIR and $INSTALL_DIR"
fi

echo "--------------------------------------------------------------------------------"
echo "running benchmarks"
echo "--------------------------------------------------------------------------------"

echo ""
echo "benchmarks with fixed num steps..."

cd $INSTALL_DIR/bin

function benchmark_run {
  ./nbs --benchmark-mode \
    --num-benchmark-steps $BENCHMARK_STEPS \
    --simulation-backend $1 \
    --num-random-bodies $2 \
    --use-random-body-gen \
    --rng-seed make_benchmark_suite_seeds_great_again
}

function benchmark_block {
  echo "benchmark block for backend $1 ($2/$3)"

  benchmark_run $1 100
  benchmark_run $1 1000
  benchmark_run $1 10000
  benchmark_run $1 100000
  benchmark_run $1 1000000
}

for i in $(seq 1 $NUM_REPEATS); do benchmark_block Sequential $i $NUM_REPEATS; done
for i in $(seq 1 $NUM_REPEATS); do benchmark_block OpenMP $i $NUM_REPEATS; done
for i in $(seq 1 $NUM_REPEATS); do benchmark_block OpenCL $i $NUM_REPEATS; done
