add_executable(nbs_app nbs.cpp)

FetchContent_Declare(json
    GIT_REPOSITORY https://github.com/nlohmann/json.git
    GIT_TAG v3.7.3)
FetchContent_MakeAvailable(json)

FetchContent_Declare(lyra
    GIT_REPOSITORY https://github.com/bfgroup/Lyra.git
    GIT_TAG 1.4)
FetchContent_MakeAvailable(lyra)

target_link_libraries(nbs_app
    PRIVATE
    nbs_lib
    nlohmann_json::nlohmann_json
    BFG::Lyra)

set_target_properties(nbs_app PROPERTIES OUTPUT_NAME "nbs")

install(TARGETS nbs_app DESTINATION bin)
