#include "SimulatorApp.hpp"

int main(int argc, char *argv[]) {
    return SimulatorApp::run(argc, argv);
}
