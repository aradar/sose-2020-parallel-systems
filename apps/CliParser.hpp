#pragma once

#include <lyra/lyra.hpp>

/**
 * Simple struct used to return the parsed command line arguments.
 */
struct CliArgs {
    bool showHelp = false;
    bool useRandomBodyGen = false;
    bool benchmarkMode = false;
    bool allowCollisions = false;
    bool printOpenClPlatforms = false;
    bool printOpenClDevices = false;
    uint numberOfRandomBodies = 100;
    uint numBenchmarkSteps = 1000;
    uint visNumStepsPerFrame = 100;
    int openClPlatformId = 0;
    int openClDeviceId = 0;
    float collisionDistance = 1e-4;
    float simulationTimeStep = 0.01;
    float minRngMass = 1.0f;
    float maxRngMass = 2e10f;
    float minRngPosition = -30.f;
    float maxRngPosition = 30.f;
    float minRngVelocity = -0.025f;
    float maxRngVelocity = 0.025f;
    std::string bodiesJsonFile;
    std::string simulationBackend = "Sequential";
    std::string rngSeed;
};

/**
 * Small Class which wraps the lyra parser configuration and parsing.
 */
class CliParser {

public:
    /**
     * Parses the given arguments and returns the parsed arguments as a
     * CliArgs struct.
     * @param argc number of elements in argv.
     * @param argv received arguments.
     * @return parsed arguments as a CLIArgs struct.
     */
    static CliArgs parse(int argc, char **argv) {
        auto cli = lyra::cli_parser();
        CliArgs parsed_args;

        cli.add_argument(lyra::help(parsed_args.showHelp));
        cli.add_argument(lyra::opt(parsed_args.useRandomBodyGen)
                .name("-r")
                .name("--use-random-body-gen")
                .help("Enables random body generation. If this is not set "
                      "-b/--bodies-json-file must be supplied"));
        cli.add_argument(lyra::opt(parsed_args.numberOfRandomBodies, "num-random-bodies")
                .name("-n")
                .name("--num-random-bodies")
                .help("Defines how many random bodies get generated."));
        cli.add_argument(lyra::opt(parsed_args.bodiesJsonFile, "bodies-json-file")
                .name("-b")
                .name("--bodies-json-file")
                .help("A JSON input file which contains the bodies for the simulation."));
        cli.add_argument(lyra::opt(parsed_args.benchmarkMode)
                .name("--benchmark-mode")
                .help("Enables the benchmark mode. If enabled the visualization "
                      "will be disabled and --num-benchmark-steps will be "
                      "simulated and the time the simulation took will be printed "
                      "in seconds."));
        cli.add_argument(lyra::opt(parsed_args.numBenchmarkSteps, "num-benchmark-steps")
                .name("--num-benchmark-steps")
                .help("Defines the number of steps the benchmark mode calculates "
                      "before exiting."));
        cli.add_argument(lyra::opt(parsed_args.simulationBackend, "simulation-backend")
                .name("--simulation-backend")
                .choices("Sequential", "OpenMP", "OpenCL")
                .help("Defines the Simulator class which gets used for the "
                      "simulation."));
        cli.add_argument(lyra::opt(parsed_args.allowCollisions)
                .name("--allow-collisions")
                .help("If activated bodies to collide and merge if they are "
                      "closer than --collision-distance."));
        cli.add_argument(lyra::opt(parsed_args.collisionDistance, "collision-distance")
                .name("--collision-distance")
                .help("Sets the distance at which bodies get merged if "
                      "--allow-collisions is activated."));
        cli.add_argument(lyra::opt(parsed_args.simulationTimeStep, "sim-time-step")
                .name("--sim-time-step")
                .help("Defines the used time step for the simulation."));
        cli.add_argument(lyra::opt(parsed_args.visNumStepsPerFrame, "vis-num-steps-per-frame")
                .name("--vis-num-steps-per-frame")
                .help("Defines how many steps a simulated between drawn frames."));
        cli.add_argument(lyra::opt(parsed_args.rngSeed, "rng-seed")
                .name("--rng-seed")
                .help("Defines the seed used during random body generation. If "
                      "the string is empty it will just use non deterministic "
                      "random number generator if possible."));
        cli.add_argument(lyra::opt(parsed_args.minRngMass, "min-rng-mass")
                .name("--min-rng-mass")
                .help("Defines the minimum value during random body mass "
                      "generation"));
        cli.add_argument(lyra::opt(parsed_args.maxRngMass, "max-rng-mass")
                .name("--max-rng-mass")
                .help("Defines the maximum value during random body mass "
                      "generation"));
        cli.add_argument(lyra::opt(parsed_args.minRngPosition, "min-rng-pos")
                .name("--min-rng-pos")
                .help("Defines the minimum value during random body position "
                      "generation"));
        cli.add_argument(lyra::opt(parsed_args.maxRngPosition, "max-rng-pos")
                .name("--max-rng-pos")
                .help("Defines the maximum value during random body position "
                      "generation"));
        cli.add_argument(lyra::opt(parsed_args.minRngVelocity, "min-rng-vel")
                .name("--min-rng-vel")
                .help("Defines the minimum value during random body velocity "
                      "generation"));
        cli.add_argument(lyra::opt(parsed_args.maxRngVelocity, "max-rng-vel")
                .name("--max-rng-vel")
                .help("Defines the maximum value during random body velocity "
                      "generation"));
        cli.add_argument(lyra::opt(parsed_args.openClPlatformId, "opencl-platform-id")
                .name("--opencl-platform-id")
                .help("Defines the OpenCL platform used by the OpenCL backend."));
        cli.add_argument(lyra::opt(parsed_args.openClDeviceId, "opencl-device-id")
                .name("--opencl-device-id")
                .help("Defines the OpenCL device used by the OpenCL backend."));
        cli.add_argument(lyra::opt(parsed_args.printOpenClPlatforms)
                .name("--print-opencl-platforms")
                .help("Prints available OpenCL platforms and exits."));
        cli.add_argument(lyra::opt(parsed_args.printOpenClDevices)
                .name("--print-opencl-devices")
                .help("Prints available OpenCL devices and exits."));

        auto result = cli.parse({argc, argv});
        if (!result) {
            std::cerr << "Error in command line: " << result.errorMessage() << std::endl;
            exit(1);
        }

        if (parsed_args.showHelp) {
            std::cout << cli << std::endl;
            exit(1);
        }

        return parsed_args;
    }

};

