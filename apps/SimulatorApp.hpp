#include <random>
#include <iostream>
#include <fstream>
#include <chrono>

#include <nlohmann/json.hpp>

#include "nbs/SequentialSimulator.hpp"
#include "nbs/OpenMpSimulator.hpp"
#include "nbs/OpenClSimulator.hpp"
#include "nbs/Visualizer.hpp"

#include "CliParser.hpp"

using json = nlohmann::json;

/**
 *
 */
class SimulatorApp {

private:

    /**
     * Small helper function to read a JSON formated file with user defined
     * bodies.
     *
     * @param json_path path as a string to the JSON file.
     * @param massScaling float value with which the mass of the bodies gets
     *      rescaled.
     * @return a std::vector with the imported Body structs.
     */
    static std::vector<Body> getBodiesFromJson(const std::string& json_path, double massScaling) {

        std::ifstream input_stream(json_path);
        json json_obj;
        input_stream >> json_obj;

        std::vector<Body> bodies;
        for (auto &element : json_obj["bodies"]) {
            auto name = element["name"].get<std::string>();

            float mass = element["mass"].get<float>() / massScaling;

            std::vector<float> position;
            for (auto &val : element["position"]) {
                position.push_back(val.get<float>());
            }

            std::vector<float> velocity;
            for (auto &val : element["velocity"]) {
                velocity.push_back(val.get<float>());
            }

            bodies.push_back({
                    glm::vec3(position[0], position[1], position[2]),
                    glm::vec3(velocity[0], velocity[1], velocity[2]),
                    mass
            });
        }

        return bodies;
    }

    /**
     * Small helper function to generate bodies randomly in the range of the
     * given arguments.
     *
     * @param number amount of bodies to generate.
     * @param minMass min possible mass value.
     * @param maxMass max possible mass value.
     * @param minPosition min possible position value per axis.
     * @param maxPosition max possible position value per axis.
     * @param minVelocity min possible velocity value per axis.
     * @param maxVelocity max possible velocity value per axis.
     * @param rngSeed a user defined seed used to seed the rng algorithm.
     * @return a std::vector with number randomly generated Body structs.
     */
    static std::vector<Body> generateRandomBodies(
            int number,
            float minMass,
            float maxMass,
            float minPosition,
            float maxPosition,
            float minVelocity,
            float maxVelocity,
            std::string &rngSeed) {

        std::vector<Body> generatedBodies;

        std::mt19937 gen;
        if (!rngSeed.empty()) {
            std::seed_seq seed(rngSeed.begin(), rngSeed.end());
            gen.seed(seed);
        } else {
            std::random_device rd;
            gen.seed(rd());
        }

        std::uniform_real_distribution<> massDis(minMass, maxMass);
        std::uniform_real_distribution<> positionDis(minPosition, maxPosition);
        std::uniform_real_distribution<> velocityDis(minVelocity, maxVelocity);

        for (int i = 0; i < number; i++) {
            generatedBodies.push_back({
                    glm::vec3(positionDis(gen), positionDis(gen), positionDis(gen)),
                    glm::vec3(velocityDis(gen), velocityDis(gen), velocityDis(gen)),
                    static_cast<float>(massDis(gen))
            });
        }
        return generatedBodies;
    }

    /**
     * Small helper function to build a simulator instance based on a string.
     * if simulatorName is not in ["Sequential", "OpenMP", "OpenCL"] a
     * std::runtime_error will be thrown.
     *
     * @param simulatorName name of the simulator to build.
     * @param bodies bodies used for the simulation.
     * @param gravitationalConst gravitational constant used for the
     *      simulation.
     * @param allowCollisions defines if collisions are allowed for the
     *      simulation.
     * @param collisionDistance defines the distance at which objects are
     *      considered as collided.
     * @param openClPlatformId id of the OpenCL platform to use if the
     *      OpenClSimulator has been chosen.
     * @param openClDeviceId id of the OpenCL device to use if the
     *      OpenClSimulator has been chosen.
     * @return a smart pointer containing the generated simulator.
     */
    static std::shared_ptr<Simulator> constructSimulator(
            std::string &simulatorName,
            std::vector<Body> &bodies,
            float gravitationalConst,
            bool allowCollisions,
            float collisionDistance,
            int openClPlatformId,
            int openClDeviceId) {

        if (simulatorName == "Sequential") {
            return std::shared_ptr<Simulator>(new SequentialSimulator(
                    bodies,
                    gravitationalConst,
                    allowCollisions,
                    collisionDistance));
        } else if (simulatorName == "OpenMP") {
            return std::shared_ptr<Simulator>(new OpenMpSimulator(
                    bodies,
                    gravitationalConst,
                    allowCollisions,
                    collisionDistance));
        } else if (simulatorName == "OpenCL") {
            return std::shared_ptr<Simulator>(new OpenClSimulator(
                    bodies,
                    gravitationalConst,
                    allowCollisions,
                    collisionDistance,
                    openClPlatformId,
                    openClDeviceId));
        }

        throw std::runtime_error("received a not supported simulation backend");
    }

    /**
     * A small helper function which benchmarks the given simulator for
     * numBenchmarkSteps.
     *
     * @param simulator simulator to benchmark.
     * @param numBenchmarkSteps number of steps the benchmark runs.
     * @param timeStep time step used for each simulation step.
     */
    static void doBenchmark(
            Simulator &simulator,
            uint numBenchmarkSteps,
            float timeStep) {

        auto startTime = std::chrono::high_resolution_clock::now();

        for (size_t i = 0; i < numBenchmarkSteps; ++i) {
            simulator.update(timeStep);
        }
        auto endTime = std::chrono::high_resolution_clock::now();
        std::chrono::duration<double> timeDiff = endTime - startTime;

        std::cout
                << "simulation of "
                << numBenchmarkSteps
                << " steps for "
                << simulator.getPositionBuffer().size()
                << " bodies took "
                << timeDiff.count()
                << " seconds"
                << std::endl;
    }

    /**
     * Small helper function which builds a Visualizer with which it
     * visualizes the given Simulator.
     *
     * @param simulator simulator which should get visualized.
     * @param numStepsPerFrame number of steps the simulation should run
     *      between frames.
     * @param timeStep time step used for each simulation step.
     */
    static void doVisualization(
            Simulator &simulator,
            uint numStepsPerFrame,
            float timeStep) {

        auto lastTime = std::chrono::high_resolution_clock::now();
        auto &posBuffer = simulator.getPositionBuffer();
        auto &massBuffer = simulator.getMassBuffer();
        auto visualizer = Visualizer(posBuffer, massBuffer);
        while (visualizer.isWindowOpen()) {
            auto currTime = std::chrono::high_resolution_clock::now();
            std::chrono::duration<double> timeDiff = currTime - lastTime;
            auto deltaTime = timeDiff.count();
            lastTime = currTime;

            for (size_t i = 0; i < numStepsPerFrame; ++i) {
                simulator.update(timeStep);
            }
            //printList(buffer);
            visualizer.draw(deltaTime);
        }

        visualizer.free();
    }

public:

    /**
     * Entry point of the whole application. It parses the user input and
     * and does everything to fulfill the requested task.
     *
     * @param argc number of elements in argv.
     * @param argv received arguments.
     * @return 0 if everything was ok and != 0 if not.
     */
    static int run(int argc, char *argv[]) {
        auto parsed_args = CliParser::parse(argc, argv);

        if (parsed_args.printOpenClPlatforms) {
            OpenClSimulator::printAvailablePlatforms();
            return 0;
        }
        if (parsed_args.printOpenClDevices) {
            OpenClSimulator::printAvailableDevices(parsed_args.openClPlatformId);
            return 0;
        }

        // todo: do something with those values
        auto timeScaling = std::pow(60.f * 60.f * 24.f, 2.f);
        auto distanceScaling = std::pow(1.496e+11f, 3);
        auto massScaling = 1e20;
        auto gravitationalConst = 6.673E-11f; // m^3 / (kg * s ^ 2)
        gravitationalConst = gravitationalConst * massScaling * timeScaling / distanceScaling;

        if (!parsed_args.useRandomBodyGen && parsed_args.bodiesJsonFile.empty()) {
            std::cerr << "either --use-random-body-gen or --bodies-json-file must be used!" << std::endl;
            exit(1);
        }

        std::vector <Body> bodies;
        if (parsed_args.useRandomBodyGen) {
            bodies = generateRandomBodies(
                    parsed_args.numberOfRandomBodies,
                    parsed_args.minRngMass,
                    parsed_args.maxRngMass,
                    parsed_args.minRngPosition,
                    parsed_args.maxRngPosition,
                    parsed_args.minRngVelocity,
                    parsed_args.maxRngVelocity,
                    parsed_args.rngSeed);
        } else {
            bodies = SimulatorApp::getBodiesFromJson(
                    parsed_args.bodiesJsonFile,
                    massScaling);
        }

        auto simulator = SimulatorApp::constructSimulator(
                parsed_args.simulationBackend,
                bodies,
                gravitationalConst,
                parsed_args.allowCollisions,
                parsed_args.collisionDistance,
                parsed_args.openClPlatformId,
                parsed_args.openClDeviceId);

        if (parsed_args.benchmarkMode) {
            SimulatorApp::doBenchmark(
                    *simulator,
                    parsed_args.numBenchmarkSteps,
                    parsed_args.simulationTimeStep);
        } else {
            SimulatorApp::doVisualization(
                    *simulator,
                    parsed_args.visNumStepsPerFrame,
                    parsed_args.simulationTimeStep);
        }

        return 0;
    }
};
