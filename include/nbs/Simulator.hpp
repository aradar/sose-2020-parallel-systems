#pragma once

#include <vector>
#include <iostream>

#include <glm/glm.hpp>
#include <glm/vec3.hpp>
#include <glm/gtx/norm.hpp>

/**
 * Simple struct to combine all properties which define a body for the
 * simulation.
 */
struct Body {
    /**
     * Position of the body as a float vector with 3 elements.
     */
    glm::vec3 position;
    /**
     * Velocity of the body as a float vector with 3 elements.
     */
    glm::vec3 velocity;
    /**
     * Mass of the body as a float.
     */
    float mass;
};

/**
 * Base class for all simulators and is therefor the public API for all
 * simulators. During construction it copies the given bodies and initialises
 * all important fields.
 *
 * The bodies and the gravitational constant must already be rescaled to be
 * computable (no overflows) and compatible (used units must fit) before
 * giving it to a Simulator instance. Otherwise the computation will run into
 * nan's.
 */
class Simulator {
protected:
    /**
     * Defines if during the simulation collisions will be handled or ignored.
     */
    const bool collisionsAllowed;
    /**
     * Distance at which two bodies are considered as collided.
     */
    const float collisionDistance;
    /**
     * Gravitational constant used for the calculations during the simulation.
     * The real constant with si units is as follows:
     * \f$ 6.67408 \times 10^{-11}m^3kg^{-1}s^{-2}\f$.
     */
    const float gravitationalConst;
    /**
     * A factor to prevent division with zero during computation and to define
     * a minimal distance between two bodies during the computation.
     */
    const float softeningFactor;
    /**
     * Internal work copy of the initial bodies.
     */
    std::vector<Body> bodies;
    /**
     * Vector of the positions of the bodies. This is the source of the
     * getPositionBuffer() function.
     */
    std::vector<glm::vec3> positions;
    /**
     * Vector of the masses of the bodies. This is the source of the
     * getMassBuffer() function.
     */
    std::vector<float> masses;

public:
    /**
     * @param bodies initial bodies vector.
     * @param gravitationalConst gravitational constant used for the
     *        calculations during the simulation.
     * @param collisionsAllowed definition if collision should be handled
     *        during simulation.
     * @param collisionDistance distance at which bodies are considered as
     *        collided.
     */
    explicit Simulator(
            const std::vector<Body> &bodies,
            float gravitationalConst,
            bool collisionsAllowed,
            float collisionDistance)
            : collisionsAllowed(collisionsAllowed),
              collisionDistance(collisionDistance),
              gravitationalConst(gravitationalConst),
              softeningFactor(1e-4),
              bodies(std::vector<Body>(bodies)),
              positions(std::vector<glm::vec3>(bodies.size())),
              masses(std::vector<float>(bodies.size())) {

        for (size_t i = 0; i < bodies.size(); ++i) {
            positions[i] = bodies[i].position;
            masses[i] = bodies[i].mass;
        }
    }

    /**
     * Calculates the update of the simulation for the given time value. A
     * common value for time is 0.01 (smaller values result in more accurate
     * simulation).
     * @param time size of the time step used for the calculation
     */
    virtual void update(float time) = 0;

    /**
     * Returns a reference to a buffer which always contains the current
     * positions of the bodies in the simulation.
     *
     * @return A reference to a buffer which always contains the current
     *         positions of the bodies in the simulation.
     */
    std::vector<glm::vec3> &getPositionBuffer() {
        return positions;
    }

    /**
     * Returns a reference to a buffer which always contains the current
     * masses of the bodies in the simulation.
     *
     * @return A reference to a buffer which always contains the current
     *         masses of the bodies in the simulation.
     */
    std::vector<float> &getMassBuffer() {
        return masses;
    }

    /**
     * Cleanup function to free eventually acquired resources for the
     * simulation. This should always be called before shutting down a
     * Simulator.
     */
    virtual void freeResources() {}
};

