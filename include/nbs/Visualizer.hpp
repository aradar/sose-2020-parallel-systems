#pragma once

#include <vector>
#include <cmath>

#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "nbs/OpenGlShaders.hpp"

class Visualizer {
private:
    constexpr static float SPEED = 20;
    constexpr static float MOUSE_SENSITIVITY = 0.1;
    constexpr static float FOV = 75.0;
    int windowWidth;
    int windowHeight;
    GLuint vbo;
    GLuint mbo;
    GLuint vao;
    GLuint shaderProgram;
    float yaw;
    float pitch;
    float lastMouseXPos;
    float lastMouseYPos;
    GLFWwindow *window = nullptr;
    glm::vec3 cameraPosition;
    glm::vec3 cameraFront;
    glm::vec3 cameraUp{};
    glm::vec3 cameraRight{};
    glm::vec3 worldUp;
    glm::mat4 cameraViewMatrix{};
    glm::mat4 projectionMatrix{};
    std::vector<glm::vec3> &posBuffer;
    std::vector<float> &massBuffer;

    /**
     * Small utility function which initializes GLFW and sets corresponding
     * variables.
     */
    void initWindow();

    /**
     * Small utility function which initializes opengl and sets corresponding
     * variables.
     */
    void initOpenGl();

    /**
     * Small utility updates the camera vector variables based on the mouse
     * movement.
     */
    void updateCameraVectors();

    /**
     * Grabs the mouse movement and translates it into a rotation and calls
     * the updateCameraVectors function after this.
     */
    void updateRotation();

    /**
     * A callback to update the rendering context if the window gets resized.
     */
    void framebufferSizeCallback(int width, int height);

    /**
     * Grabs the keyboard input and updates the position of the camera with
     * this.
     */
    void updateTranslation(float deltaTime);

public:
    explicit Visualizer(std::vector<glm::vec3> &posBuffer, std::vector<float> &massBuffer)
            : windowWidth(800),
              windowHeight(600),
              vbo(0),
              mbo(0),
              vao(0),
              shaderProgram(0),
              yaw(-90),
              pitch(0),
              lastMouseXPos(-1),
              lastMouseYPos(-1),
              window(nullptr),
              cameraPosition(0, 0, 5),
              cameraFront(0, 0, -1),
              worldUp(0, 1, 0),
              posBuffer(posBuffer),
              massBuffer(massBuffer) {

        initWindow();
        initOpenGl();
    }

    /**
     * Checks if the window is still open and returns the result.
     * @return True if the window is still open.
     */
    bool isWindowOpen();

    /**
     * Draws the next frame with the values in the buffers.
     * @param deltaTime time since last update to smooth out the movement
     *      on slower machines.
     */
    void draw(float deltaTime);

    /**
     * Frees acquired resources. This must always be called before shutting
     * down the visualizer.
     */
    void free();
};
