#pragma once

const auto nBodyForLoopKernelSources = R""(
typedef struct tag_Body {
    float3 position;
    float3 velocity;
    float mass;
} Body;

__kernel void calcAccelerations(__global Body* bodies, int bodiesSize, float softeningFactor, float gravitationalConst, global float3* accelerations) {
    size_t i = get_global_id(0);
    Body curr = bodies[i];
    float3 receivedForce = (float3)(0.0f);
    for (size_t j = 0; j < bodiesSize; ++j) {
        if (j == i) {
            continue;
        }

        Body other = bodies[j];

        // all pow operations in this code have been replaced as using
        // pow causes "randomly" segserv errors in a certain not clear
        // situation
        float3 posDiff = other.position - curr.position;
        float lenOfPosDif = length(posDiff);
        float sqLenOfPosDiff = lenOfPosDif * lenOfPosDif + softeningFactor * softeningFactor;
        // (sqLenOfPosDiff * sqrt(sqLenOfPosDiff)) == pow(sqLenOfPosDiff, 3 / 2.f)
        receivedForce += (other.mass * posDiff) / (sqLenOfPosDiff * sqrt(sqLenOfPosDiff));
    }
    accelerations[i] = gravitationalConst * receivedForce;
}

__kernel void applyAccelerations(__global Body* bodies, __global float3* accelerations, float time, __global float3* positions){
    size_t i = get_global_id(0);
    Body curr = bodies[i];

    // physical vel and pos update
    float3 accel = accelerations[i];
    // p = 0.5 * a * t^2 + v_0 * t + p_0
    curr.position = 0.5f * accel * time * time + curr.velocity * time + curr.position;
    // v = a * t + v_0
    curr.velocity = accel * time + curr.velocity;

    bodies[i] = curr;
    positions[i] = curr.position;
}
)"";
