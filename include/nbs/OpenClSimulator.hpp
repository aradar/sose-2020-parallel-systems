#pragma once

// version > 120 break nvidia based systems and setting the target version is
// not possible without also setting the minimum version
#define CL_HPP_MINIMUM_OPENCL_VERSION 120
#define CL_HPP_TARGET_OPENCL_VERSION 120
#define CL_HPP_ENABLE_EXCEPTIONS

#include <CL/cl2.hpp>

#include "nbs/Simulator.hpp"
#include "nbs/OpenClKernels.hpp"

/**
 * Simple alternative bodies struct which uses OpenCL defined types. This is
 * required as the normal Body struct can't be used without having to add
 * empty byte alignment fields.
 */
struct ClBody {
    cl_float3 position;
    cl_float3 velocity;
    cl_float mass;
};

/**
 * A implementation of the brute force algorithm for the simulation using
 * OpenCL to speedup the \f$O(n^2)\f$ computation by running it in parallel.
 */
class OpenClSimulator : public Simulator{
    cl::Context context;
    cl::Platform platform;
    cl::Device device;
    cl::Program::Sources sources;
    cl::CommandQueue queue;
    cl::Program program;
    int platformId;
    int deviceId;
    // the following two vectors are replacements for the non OpenCL
    // variables. As those are not usable with OpenCL
    std::vector<ClBody> clBodies;
    std::vector<cl_float3> clPositions;
    cl::Buffer bodiesBuffer;
    cl::Buffer accelerationsBuffer;
    cl::Buffer positionsBuffer;

    /**
     * Simple utility function which initialises all OpenCL based variables
     * and states.
     */
    void initOpenCl();
    /**
     * Simple utility function which writes the cl_float3 values from the
     * std::vector<cl_float3> back into the normal position buffer of type
     * std::vector<glm::vec3>.
     */
    void convertPositions();
    /**
     * Simple utility function which converts the normal Body structs into
     * ClBody structs.
     * @param bodies the bodies which should get converted
     * @return the converted bodies
     */
    std::vector<ClBody> convertBodies(const std::vector<Body> &bodies);

    /**
     * Checks if bodies currently collied and if so merges them.
     */
    void handleCollisions();
    /**
     * Calculates the acceleration every body receives from all other bodies.
     */
    void calcAccelerations();
    /**
     * Applies the previously calculated acceleration values to all bodies.
     */
    void applyAccelerations(float time);

public:
    /**
     * @param bodies initial bodies vector.
     * @param gravitationalConst gravitational constant used for the
     *      calculations during the simulation.
     * @param collisionsAllowed definition if collision should be handled
     *      during simulation.
     * @param collisionDistance distance at which bodies are considered as
     *      collided.
     * @param platformId ID of the OpenCL platform to be used for the
     *      simulation calculation.
     * @param deviceId ID of the OpenCL device to be used for the
     *      simulation calculation.
     */
    explicit OpenClSimulator(
            const std::vector<Body> &bodies,
            float gravitationalConst,
            bool collisionsAllowed,
            float collisionDistance,
            int platformId = 0,
            int deviceId = 0)
            : Simulator(bodies, gravitationalConst, collisionsAllowed, collisionDistance),
              context(),
              platform(),
              device(),
              sources(),
              queue(),
              program(),
              platformId(platformId),
              deviceId(deviceId),
              clBodies(convertBodies(bodies)),
              clPositions(std::vector<cl_float3>(clBodies.size())) {

        initOpenCl();
    }

    void update(float time) override;

    /**
     * Small helper function which simply prints all for the application
     * currently visible OpenCL platforms.
     */
    static void printAvailablePlatforms();

    /**
     * Small helper function which simply prints all for the application
     * with the given platform currently visible OpenCL devices.
     * @param platformId id for which the devices should be printed.
     */
    static void printAvailableDevices(int platformId);

    /**
     * Small helper function which simply prints all for the application
     * with the given platform currently visible OpenCL devices.
     * @param platform cl::Platform object for which the devices should be
     *      printed.
     */
    static void printAvailableDevices(const cl::Platform &platform);

    void freeResources() override;
};


