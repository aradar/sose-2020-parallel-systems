#pragma once

#include "nbs/Simulator.hpp"

/**
 * A implementation of the brute force algorithm for the simulation using
 * OpenMP to speedup the \f$O(n^2)\f$ computation by running it in parallel.
 */
class OpenMpSimulator: public Simulator {

    /**
     * A scratch buffer for the computation.
     */
    std::vector<glm::vec3> accelerations;

    /**
     * Checks if bodies currently collied and if so merges them.
     */
    void handleCollisions();
    /**
     * Calculates the acceleration every body receives from all other bodies.
     */
    void calcAccelerations();
    /**
     * Applies the previously calculated acceleration values to all bodies.
     */
    void applyAccelerations(float time);

public:
    /**
     * @param bodies initial bodies vector.
     * @param gravitationalConst gravitational constant used for the
     *        calculations during the simulation.
     * @param collisionsAllowed definition if collision should be handled
     *        during simulation.
     * @param collisionDistance distance at which bodies are considered as
     *        collided.
     */
    explicit OpenMpSimulator(
            const std::vector<Body> &bodies,
            float gravitationalConst,
            bool collisionsAllowed,
            float collisionDistance)
            : Simulator(bodies, gravitationalConst, collisionsAllowed, collisionDistance),
              accelerations(bodies.size()) {}

    void update(float time) override;
};