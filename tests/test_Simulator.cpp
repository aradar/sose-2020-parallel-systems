#include <catch2/catch.hpp>

#include <glm/glm.hpp>
#include <glm/vec3.hpp>
#include <glm/gtx/norm.hpp>

#include <nbs/OpenClSimulator.hpp>
#include "nbs/SequentialSimulator.hpp"
#include "nbs/OpenMpSimulator.hpp"
#include "nbs/OpenClSimulator.hpp"

static const auto timeScaling = std::pow(60.f * 60.f * 24.f, 2.f);
static const auto distanceScaling = std::pow(1.496e+11f, 3);
static const auto massScaling = 1e20;
static const auto gravitationalConst = 6.673E-11f * massScaling * timeScaling / distanceScaling; // m^3 / (kg * s ^ 2)

static const std::vector<Body> stableTwoBodySystem {
        { // star
                glm::vec3(0.0, 0.0, 0.0),
                glm::vec3(0.0, 0.0, 0.0),
                static_cast<float>(2.0e+30 / massScaling)},
        { // planet
                glm::vec3(0,  -1,  0.0),
                glm::vec3(0.0175, 0.0,  0.0),
                static_cast<float>(6.0e+24 / massScaling)}
};

static const std::vector<Body> stableThreeBodySystem {
        { // star
                glm::vec3(0.0, 0.0, 0.0),
                glm::vec3(0.0, 0.0, 0.0),
                static_cast<float>(2.0e+30 / massScaling)},
        { // planet
                glm::vec3(0,  -1,  0.0),
                glm::vec3(0.02, 0.0,  0.0),
                static_cast<float>(6.0e+24 / massScaling)},
        { // planet
                glm::vec3(0,  1,  0.0),
                glm::vec3(-0.02, 0.0,  0.0),
                static_cast<float>(6.0e+24 / massScaling)}
};

static const std::vector<Body> stableFourBodySystem {
        { // star
                glm::vec3(0.0, 0.0, 0.0),
                glm::vec3(0.0, 0.0, 0.0),
                static_cast<float>(2.0e+30 / massScaling)},
        { // planet
                glm::vec3(0,  -1,  0.0),
                glm::vec3(0.02, 0.0,  0.0),
                static_cast<float>(6.0e+24 / massScaling)},
        { // planet
                glm::vec3(0,  1,  0.0),
                glm::vec3(-0.02, 0.0,  0.0),
                static_cast<float>(6.0e+24 / massScaling)},
        { // planet
                glm::vec3(-1,  0,  0.0),
                glm::vec3(0.0, 0.02,  0.0),
                static_cast<float>(6.0e+24 / massScaling)},
        { // planet
                glm::vec3(1,  0,  0.0),
                glm::vec3(0.0, -0.02,  0.0),
                static_cast<float>(6.0e+24 / massScaling)}
};

static const std::vector<Body> stablePlanetOnlyTwoBodySystem {
        { // planet
                glm::vec3(0,  -1,  0.0),
                glm::vec3(0.019, 0.0,  0.0),
                static_cast<float>(1.0e+31 / massScaling)},
        { // planet
                glm::vec3(0,  1,  0.0),
                glm::vec3(-0.019, 0.0,  0.0),
                static_cast<float>(1.0e+31 / massScaling)}
};

static const std::vector<Body> unstableTwoBodySystem {
        { // planet
                glm::vec3(0,  -1,  0.0),
                glm::vec3(0.019, 0.0,  0.0),
                static_cast<float>(1.0e+24 / massScaling)},
        { // planet
                glm::vec3(0,  1,  0.0),
                glm::vec3(-0.019, 0.0,  0.0),
                static_cast<float>(1.0e+24 / massScaling)}
};

static const std::vector<Body> unstableFourBodySystem {
        { // planet
                glm::vec3(0,  -1,  0.0),
                glm::vec3(0.02, 0.0,  0.0),
                static_cast<float>(6.0e+24 / massScaling)},
        { // planet
                glm::vec3(0,  1,  0.0),
                glm::vec3(-0.02, 0.0,  0.0),
                static_cast<float>(6.0e+24 / massScaling)},
        { // planet
                glm::vec3(-1,  0,  0.0),
                glm::vec3(0.0, 0.02,  0.0),
                static_cast<float>(6.0e+24 / massScaling)},
        { // planet
                glm::vec3(1,  0,  0.0),
                glm::vec3(0.0, -0.02,  0.0),
                static_cast<float>(6.0e+24 / massScaling)}
};

static const std::vector<Body> collidingTwoBodySystem {
        { // planet
                glm::vec3(1.5,  0,  0.0),
                glm::vec3(-0.05, 0.0,  0.0),
                static_cast<float>(1.0e+24 / massScaling)},
        { // planet
                glm::vec3(-1.5,  0,  0.0),
                glm::vec3(0.05, 0.0,  0.0),
                static_cast<float>(1.0e+24 / massScaling)}
};

static const std::vector<Body> collidingFourBodySystem {
        { // planet
                glm::vec3(1.5,  0,  0.0),
                glm::vec3(-0.05, 0.0,  0.0),
                static_cast<float>(1.0e+24 / massScaling)},
        { // planet
                glm::vec3(-1.5,  0,  0.0),
                glm::vec3(0.05, 0.0,  0.0),
                static_cast<float>(1.0e+24 / massScaling)},
        { // planet
                glm::vec3(0,  1.5,  0.0),
                glm::vec3(0, -0.05,  0.0),
                static_cast<float>(1.0e+24 / massScaling)},
        { // planet
                glm::vec3(0,  -1.5,  0.0),
                glm::vec3(0, 0.05,  0.0),
                static_cast<float>(1.0e+24 / massScaling)}
};

static const std::vector<Body> farTwoBodySystem {
        { // planet
                glm::vec3(1.5,  0,  0.0),
                glm::vec3(0.05, 0.0,  0.0),
                static_cast<float>(1.0e+24 / massScaling)},
        { // planet
                glm::vec3(-1.5,  0,  0.0),
                glm::vec3(-0.05, 0.0,  0.0),
                static_cast<float>(1.0e+24 / massScaling)}
};

static const std::vector<Body> farFourBodySystem {
        { // planet
                glm::vec3(1.5,  0,  0.0),
                glm::vec3(0.05, 0.0,  0.0),
                static_cast<float>(1.0e+24 / massScaling)},
        { // planet
                glm::vec3(-1.5,  0,  0.0),
                glm::vec3(-0.05, 0.0,  0.0),
                static_cast<float>(1.0e+24 / massScaling)},
        { // planet
                glm::vec3(0,  1.5,  0.0),
                glm::vec3(0, 0.05,  0.0),
                static_cast<float>(1.0e+24 / massScaling)},
        { // planet
                glm::vec3(0,  -1.5,  0.0),
                glm::vec3(0, -0.05,  0.0),
                static_cast<float>(1.0e+24 / massScaling)}
};


template<class T>
void stableSystemTest(size_t numIterations, const std::vector<Body> &bodies, int starIdx, float maxAllowedDist) {
    T simulator = T(bodies, gravitationalConst, false, 0.0);

    auto &positions = simulator.getPositionBuffer();
    auto maxDist = 0.0f;
    for (size_t i = 0; i < numIterations; ++i) {
        simulator.update(0.01);

        for (size_t j = 0; j < bodies.size(); ++j) {
            if ((int)j == starIdx) {
                continue;
            }

            auto dist = glm::distance(positions[starIdx], positions[j]);
            maxDist = std::max(dist, maxDist);
        }
    }

    REQUIRE(maxDist <= maxAllowedDist);
}

template<class T>
void unstableSystemTest(size_t numIterations, const std::vector<Body> &bodies, float minDistToCenter) {
    T simulator = T(bodies, gravitationalConst, false, 0.0);

    auto &positions = simulator.getPositionBuffer();
    for (size_t i = 0; i < numIterations; ++i) {
        simulator.update(0.01);
    }

    for (auto &pos: positions) {
        auto dist = glm::distance(pos, glm::vec3(0, 0, 0));
        CHECK(dist >= minDistToCenter);
    }
}

template<class T>
void collidingSystemTest(size_t numIterations, const std::vector<Body> &bodies) {
    T simulator = T(bodies, gravitationalConst, true, 0.1);

    float massesSum = 0;
    for (auto body : bodies){
        massesSum += body.mass;
    }

    auto &positions = simulator.getPositionBuffer();
    for (size_t i = 0; i < numIterations; ++i) {
        simulator.update(0.01);
    }

    // only one body has to be there
    CHECK(positions.size() == 1);
    // mass of merged body has to be sum of merged bodies masses
    CHECK(simulator.getMassBuffer()[0] == massesSum);
}

template<class T>
void farObjectsDontCollideTest(size_t numIterations, const std::vector<Body> &bodies) {
    T simulator = T(bodies, gravitationalConst, true, 0.05);

    auto &positions = simulator.getPositionBuffer();
    for (size_t i = 0; i < numIterations; ++i) {
        simulator.update(0.01);
    }

    CHECK(positions.size() == bodies.size());
}


TEMPLATE_TEST_CASE(
        "Testing stable systems",
        "[simulator][stable]",
        SequentialSimulator, OpenMpSimulator, OpenClSimulator) {
    // the chosen systems are not perfectly stable and do slowly drift away
    // from each other besides the drift generated by the inaccuracy of the
    // ieee floating point operations --> therefor only a number of 50000
    // iterations gets tested for this
    stableSystemTest<TestType>(5000, stableTwoBodySystem, 0, 1.1);
    stableSystemTest<TestType>(5000, stableThreeBodySystem, 0, 2.1);
    stableSystemTest<TestType>(5000, stableFourBodySystem, 0, 2.3);
    stableSystemTest<TestType>(5000, stablePlanetOnlyTwoBodySystem, -1, 1.05);
}

TEMPLATE_TEST_CASE(
        "Testing unstable systems",
        "[simulator][unstable]",
        SequentialSimulator, OpenMpSimulator, OpenClSimulator) {
    unstableSystemTest<TestType>(50000, unstableTwoBodySystem, 9);
    unstableSystemTest<TestType>(50000, unstableFourBodySystem, 9);
}

TEMPLATE_TEST_CASE(
        "Testing colliding systems with collisions allowed",
        "[simulator][colliding]",
        SequentialSimulator, OpenMpSimulator) {
    collidingSystemTest<TestType>(10000, collidingTwoBodySystem);
    collidingSystemTest<TestType>(10000, collidingFourBodySystem);
}

TEMPLATE_TEST_CASE(
        "Testing not colliding systems with collisions allowed",
        "[simulator][colliding]",
        SequentialSimulator, OpenMpSimulator) {
    farObjectsDontCollideTest<TestType>(10000, farTwoBodySystem);
    farObjectsDontCollideTest<TestType>(10000, farFourBodySystem);
}
