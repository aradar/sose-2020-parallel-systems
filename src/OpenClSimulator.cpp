
#include "nbs/OpenClSimulator.hpp"

std::vector<cl::Platform> getAvailablePlatforms() {
    std::vector<cl::Platform> platforms;
    cl::Platform::get(&platforms);

    return platforms;
}

std::vector<cl::Device> getAvailableDevices(const cl::Platform &platform) {
    std::vector<cl::Device> devices;
    platform.getDevices(CL_DEVICE_TYPE_ALL, &devices);

    return devices;
}

void OpenClSimulator::printAvailablePlatforms() {
    auto platforms = getAvailablePlatforms();

    for (size_t i = 0; i < platforms.size(); i++) {
        std::cout << "Platform " << i << ":"
                  << " name = " << platforms[i].getInfo<CL_PLATFORM_NAME>() << ","
                  << " vendor = " << platforms[i].getInfo<CL_PLATFORM_VENDOR>() << ","
                  << " version = " << platforms[i].getInfo<CL_PLATFORM_VERSION>() << ","
                  << " profile = " << platforms[i].getInfo<CL_PLATFORM_PROFILE>()
                  << std::endl;
    }
}

void OpenClSimulator::printAvailableDevices(int platformId) {
    printAvailableDevices(getAvailablePlatforms()[platformId]);
}

void OpenClSimulator::printAvailableDevices(const cl::Platform &platform) {
    auto devices = getAvailableDevices(platform);
    for (size_t i = 0; i < devices.size(); i++) {
        std::cout << "Device " << i << ":"
                  << " name = " << devices[i].getInfo<CL_DEVICE_NAME>() << ","
                  << " type = " << devices[i].getInfo<CL_DEVICE_TYPE>() << ","
                  << " max compute units = " << devices[i].getInfo<CL_DEVICE_MAX_COMPUTE_UNITS>() << ","
                  << " memory (GB) = " << devices[i].getInfo<CL_DEVICE_GLOBAL_MEM_SIZE>() / 1024. / 1024. / 1024. << ","
                  << " available = " << devices[i].getInfo<CL_DEVICE_AVAILABLE>()
                  << " address bits = " << devices[i].getInfo<CL_DEVICE_ADDRESS_BITS>()
                  << std::endl;
    }
}

void OpenClSimulator::handleCollisions() {
    throw std::runtime_error("collision handling is not implemented for the OpenCL Simulator!");
}

void OpenClSimulator::calcAccelerations() {
    cl_int bodiesSize = bodies.size();
    cl_float clSofteningFactor = softeningFactor;
    cl_float clGravitationalConst = gravitationalConst;

    cl::Kernel kernel(program, "calcAccelerations");
    kernel.setArg(0, bodiesBuffer);
    kernel.setArg(1, sizeof(cl_int), &bodiesSize);
    kernel.setArg(2, sizeof(cl_float), &clSofteningFactor);
    kernel.setArg(3, sizeof(cl_float), &clGravitationalConst);
    kernel.setArg(4, accelerationsBuffer);

    queue.enqueueNDRangeKernel(kernel, cl::NullRange, cl::NDRange(clBodies.size()));
}

void OpenClSimulator::applyAccelerations(float time) {
    cl_float clTime = time;

    cl::Kernel kernel(program, "applyAccelerations");
    kernel.setArg(0, bodiesBuffer);
    kernel.setArg(1, accelerationsBuffer);
    kernel.setArg(2, sizeof(cl_float), &clTime);
    kernel.setArg(3, positionsBuffer);

    queue.enqueueNDRangeKernel(kernel, cl::NullRange, cl::NDRange(clBodies.size()));

    queue.enqueueReadBuffer(
            positionsBuffer,
            CL_TRUE,
            0,
            clPositions.size() * sizeof(clPositions[0]),
            clPositions.data(),
            nullptr,
            nullptr);

    convertPositions();
}

void OpenClSimulator::update(float time) {
    if(collisionsAllowed){
        handleCollisions();
    }
    calcAccelerations();
    applyAccelerations(time);
}

void OpenClSimulator::initOpenCl() {
    platform = getAvailablePlatforms()[platformId];
    device = getAvailableDevices(platform)[deviceId];
    context = cl::Context(device);

    const std::string kernelsSource(nBodyForLoopKernelSources);
    sources.push_back({kernelsSource});

    program = cl::Program(context, sources);
    try {
        program.build({device});
    }
    catch (const cl::BuildError &e) {
        for (auto const &error_pair: e.getBuildLog()) {
            std::cout << error_pair.second << std::endl;
        }
        throw std::runtime_error("Compiling the kernel code caused a fatal error");
    }

    queue = cl::CommandQueue(context, device);

    auto accelerations = std::vector<cl_float3>();
    accelerations.resize(clBodies.size());

    bodiesBuffer = cl::Buffer(context, CL_MEM_READ_WRITE, clBodies.size() * sizeof(ClBody));
    accelerationsBuffer = cl::Buffer(context, CL_MEM_READ_WRITE, clBodies.size() * sizeof(cl_float3));
    positionsBuffer = cl::Buffer(context, CL_MEM_READ_WRITE, clPositions.size() * sizeof(clPositions[0]));

    queue.enqueueWriteBuffer(
            bodiesBuffer,
            CL_TRUE,
            0,
            clBodies.size() * sizeof(ClBody),
            clBodies.data(),
            nullptr,
            nullptr);
}

void OpenClSimulator::freeResources() {
    queue.finish();
}

std::vector<ClBody> OpenClSimulator::convertBodies(const std::vector<Body> &bodies) {
    std::vector<ClBody> newBodies;

    for (auto const &body: bodies) {
        newBodies.push_back({
            {{body.position[0], body.position[1], body.position[2]}},
            {{body.velocity[0], body.velocity[1], body.velocity[2]}},
            body.mass
        });
    }

    return newBodies;
}

void OpenClSimulator::convertPositions() {
    for (size_t i = 0; i < clPositions.size(); ++i) {
        positions[i] = glm::vec3(
                clPositions[i].x, clPositions[i].y, clPositions[i].z);
    }
}

