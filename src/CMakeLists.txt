file(GLOB HEADER_LIST CONFIGURE_DEPENDS "${n_body_sim_SOURCE_DIR}/include/n_body_sim/*.hpp")

add_library(nbs_lib
        SequentialSimulator.cpp
        OpenMpSimulator.cpp
        OpenClSimulator.cpp
        Visualizer.cpp
        ${HEADER_LIST})

target_include_directories(nbs_lib 
    PUBLIC 
    "${n_body_sim_SOURCE_DIR}/include")

# enable OpenMP support and make it linkable
find_package(OpenMP REQUIRED)
# add OpenCL. This requires system packages
find_package(OpenCL REQUIRED)

include_directories(${OpenCL_INCLUDE_DIR})

FetchContent_Declare(glfw
        GIT_REPOSITORY https://github.com/glfw/glfw.git
        GIT_TAG 3.3.2)
FetchContent_MakeAvailable(glfw)

FetchContent_Declare(glm
        GIT_REPOSITORY https://github.com/g-truc/glm.git
        GIT_TAG 0.9.9.8)
FetchContent_MakeAvailable(glm)

FetchContent_Declare(glad
        GIT_REPOSITORY https://github.com/Dav1dde/glad.git
        GIT_TAG v0.1.33)
FetchContent_MakeAvailable(glad)

target_link_libraries(nbs_lib
        PUBLIC
        glad
        glm
        glfw
        PRIVATE
        OpenMP::OpenMP_CXX
        ${OpenCL_LIBRARY})

set_target_properties(nbs_lib PROPERTIES OUTPUT_NAME "nbs")

install(TARGETS nbs_lib DESTINATION lib)
install(FILES ${HEADER_LIST} DESTINATION include/nbs)

source_group(TREE "${PROJECT_SOURCE_DIR}/include" PREFIX "Header Files" FILES ${HEADER_LIST})

