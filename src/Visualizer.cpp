#include "nbs/Visualizer.hpp"

#include <iostream>

void Visualizer::framebufferSizeCallback(int width, int height) {
    windowWidth = width;
    windowHeight = height;
    projectionMatrix = glm::perspective(glm::radians(FOV), windowWidth / static_cast<float>(windowHeight), 0.00001f, 1e10f);
    //projectionMatrix = glm::ortho(0.0f, static_cast<float>(windowWidth), 0.0f, static_cast<float>(windowHeight), 0.1f, 100.0f);
    glViewport(0, 0, width, height);}

void Visualizer::updateTranslation(float deltaTime) {
    float velocity = (float) SPEED * deltaTime;

    if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS) {
        cameraPosition += cameraFront * velocity;
    }
    if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS) {
        cameraPosition -= cameraFront * velocity;
    }
    if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS) {
        cameraPosition -= cameraRight * velocity;
    }
    if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS) {
        cameraPosition += cameraRight * velocity;
    }
}

void Visualizer::initWindow() {
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    //needed for MacOS X
    //glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);

    window = glfwCreateWindow(
            windowWidth,
            windowHeight,
            "N-Body-Simulator",
            nullptr,
            nullptr);
    if (window == nullptr) {
        std::cout << "Failed to create GLFW window" << std::endl;
        glfwTerminate();
        return;
    }
    glfwMakeContextCurrent(window);

    glfwSetWindowUserPointer(window, this);

    auto frameBufferSizeCallback = [](GLFWwindow *window, int width, int height)
    {
        auto userPointer = glfwGetWindowUserPointer(window);
        auto instance = static_cast<Visualizer*>(userPointer);
        instance->framebufferSizeCallback(width, height);
    };

    glfwSetFramebufferSizeCallback(window, frameBufferSizeCallback);

    glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
}

void Visualizer::initOpenGl() {
    if (!gladLoadGLLoader((GLADloadproc) glfwGetProcAddress)) {
        std::cout << "Failed to initialize GLAD" << std::endl;
        return;
    }

    framebufferSizeCallback(windowWidth, windowHeight);

    glEnable(GL_PROGRAM_POINT_SIZE);

    //generate vertex buffer
    glGenBuffers(1, &vbo);
    glGenBuffers(1, &mbo);
    //generate vertex array
    glGenVertexArrays(1, &vao);

    glBindVertexArray(vao);

    //bind buffer to GL_ARRAY_BUFFER target
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(posBuffer[0]) * posBuffer.size(), &posBuffer[0], GL_DYNAMIC_DRAW);

    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), nullptr);
    glEnableVertexAttribArray(0);

    glBindBuffer(GL_ARRAY_BUFFER, mbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(massBuffer[0]) * massBuffer.size(), &massBuffer[0], GL_DYNAMIC_DRAW);

    glVertexAttribPointer(1, 1, GL_FLOAT, GL_FALSE, sizeof(float), nullptr);
    glEnableVertexAttribArray(1);

    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);

    unsigned int vertexShader;
    //create vertex shader
    vertexShader = glCreateShader(GL_VERTEX_SHADER);
    //attach shader source code
    glShaderSource(vertexShader, 1, &vertexShaderSources, NULL);
    //compile shader
    glCompileShader(vertexShader);

    //check if compiling was successful
    int success;
    char infoLog[512];
    glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &success);
    if (!success) {
        glGetShaderInfoLog(vertexShader, 512, NULL, infoLog);
        std::cout << "ERROR::SHADER::VERTEX::COMPILATION_FAILED\n" << infoLog << std::endl;
    }

    unsigned int fragmentShader;
    fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragmentShader, 1, &fragmentShaderSources, NULL);
    glCompileShader(fragmentShader);

    glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &success);
    if (!success) {
        glGetShaderInfoLog(vertexShader, 512, NULL, infoLog);
        std::cout << "ERROR::SHADER::FRAGMENT::COMPILATION_FAILED\n" << infoLog << std::endl;
    }

    //todo:test if compilation of fragment shader worked

    shaderProgram = glCreateProgram();
    glAttachShader(shaderProgram, vertexShader);
    glAttachShader(shaderProgram, fragmentShader);
    glLinkProgram(shaderProgram);

    glGetProgramiv(shaderProgram, GL_LINK_STATUS, &success);
    if (!success) {
        glGetProgramInfoLog(shaderProgram, 512, NULL, infoLog);
        std::cout << "ERROR::SHADER::PROGRAM::LINKING_FAILED\n" << infoLog << std::endl;
    }

    glDeleteShader(vertexShader);
    glDeleteShader(fragmentShader);
}

void Visualizer::updateCameraVectors() {
    glm::vec3 tempFront;
    tempFront.x = static_cast<float>(
            std::cos(glm::radians(yaw)) * std::cos(glm::radians(pitch)));
    tempFront.y = static_cast<float>(std::sin(glm::radians(pitch)));
    tempFront.z = static_cast<float>(
            std::sin(glm::radians(yaw)) * std::cos(glm::radians(pitch)));
    cameraFront = glm::normalize(tempFront);

    cameraRight = glm::normalize(glm::cross(cameraFront, worldUp));
    cameraUp = glm::normalize(glm::cross(cameraRight, cameraFront));
}

void Visualizer::updateRotation(){
    double mouseXPos;
    double mouseYPos;
    glfwGetCursorPos(window, &mouseXPos, &mouseYPos);
    auto currMouseXPos = static_cast<float>(mouseXPos);
    auto currMouseYPos = static_cast<float >(mouseYPos);

    if (lastMouseXPos == -1 && lastMouseYPos == -1){
        lastMouseXPos = currMouseXPos;
        lastMouseYPos = currMouseYPos;
        return;
    }

    auto xDiff = currMouseXPos - lastMouseXPos;
    auto yDiff = lastMouseYPos - currMouseYPos;

    yaw += xDiff * MOUSE_SENSITIVITY;
    pitch += yDiff * MOUSE_SENSITIVITY;

    if (pitch > 89.0) {
        pitch = 89.0;
    }
    if (pitch < -89.0) {
        pitch = -89.0;
    }

    updateCameraVectors();

    lastMouseXPos = currMouseXPos;
    lastMouseYPos = currMouseYPos;
}

void Visualizer::draw(float deltaTime) {
    glfwPollEvents();

    updateTranslation(deltaTime);
    updateRotation();

    glm::vec3 targetCameraPos;
    targetCameraPos = cameraPosition + cameraFront;
    cameraViewMatrix = glm::lookAt(cameraPosition, targetCameraPos, cameraUp);
    glm::mat4 model(1.0);
    glm::mat4 mvp = projectionMatrix * cameraViewMatrix * model;

    glClear(GL_COLOR_BUFFER_BIT);
    //glClearColor(1.0f, 1.0f, 1.0f, 0.0f);

    glUseProgram(shaderProgram);
    glBindVertexArray(vao);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(posBuffer[0]) * posBuffer.size(), &posBuffer[0]);
    glBindBuffer(GL_ARRAY_BUFFER, mbo);
    glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(massBuffer[0]) * massBuffer.size(), &massBuffer[0]);

    glUniformMatrix4fv(glGetUniformLocation(shaderProgram, "mvp"), 1, GL_FALSE, &mvp[0][0]);

    glDrawArrays(GL_POINTS, 0, posBuffer.size());

    glfwSwapBuffers(window);
}

bool Visualizer::isWindowOpen() {
    if (window == nullptr) {
        return false;
    }

    return !glfwWindowShouldClose(window);
}

void Visualizer::free() {
    glDeleteVertexArrays(1, &vao);
    glDeleteBuffers(1, &vbo);
    glDeleteProgram(shaderProgram);

    glfwTerminate();
    window = nullptr;
}

