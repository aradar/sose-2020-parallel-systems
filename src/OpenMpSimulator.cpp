#include <cmath>
#include <set>
#include "nbs/OpenMpSimulator.hpp"

void OpenMpSimulator::handleCollisions() {
    for (size_t i = 0; i < bodies.size(); ++i) {
        auto &curr = bodies[i];
        std::set<size_t> mergeIndices;
        #pragma omp parallel for default(none) shared(bodies, curr, mergeIndices, i)
        for (size_t j = 0; j < bodies.size(); ++j) {
            if (j == i) {
                continue;
            }

            auto &otherBody = bodies[j];

            if (glm::distance(curr.position, otherBody.position) <= collisionDistance) {
                #pragma omp critical
                mergeIndices.insert(j);
            }
        }

        for (auto &j : mergeIndices) {
            auto &other = bodies[j];

            auto bodyForce = curr.mass * curr.velocity;
            auto otherForce = other.mass * other.velocity;
            auto combMass = curr.mass + other.mass;

            curr.velocity = (bodyForce + otherForce) / combMass;
            curr.mass = combMass;
            masses[i] = combMass;
        }

        //to parallelize for loops with openmp they must have the structure (init; condition; increment)
        //probably in the following case it is more performant not to parallelize it anyway
        for (auto rit = mergeIndices.rbegin(); rit != mergeIndices.rend(); rit++) {
            bodies.erase(bodies.begin() + *rit);
            positions.erase(positions.begin() + *rit);
            masses.erase(masses.begin() + *rit);
        }
    }
}

void OpenMpSimulator::calcAccelerations() {
    #pragma omp parallel for default(none) shared(bodies, accelerations)
    for (size_t i = 0; i < bodies.size(); ++i) {
        auto &curr = bodies[i];
        auto receivedForce = glm::vec3(0.);
        for (size_t j = 0; j < bodies.size(); ++j) {
            if (j == i) {
                continue;
            }

            auto &other = bodies[j];

            auto posDiff = other.position - curr.position;
            auto sqLenOfPosDiff = glm::length2(posDiff) + std::pow(softeningFactor, 2.f);
            receivedForce += (other.mass * posDiff) / std::pow(sqLenOfPosDiff, 3 / 2.f);
        }

        accelerations[i] = gravitationalConst * receivedForce;
    }
}

void OpenMpSimulator::applyAccelerations(float time) {
    #pragma omp parallel for default(none) shared(bodies, positions, time, accelerations)
    for (size_t i = 0; i < bodies.size(); ++i) {
        auto &curr = bodies[i];

        // simple vel and pos update
        //curr.velocity += accelerations[i] * time;
        //curr.position += curr.velocity * time;

        // physical vel and pos update
        auto accel = accelerations[i];
        // p = 0.5 * a * t^2 + v_0 * t + p_0
        curr.position = 0.5f * accel * std::pow(time, 2.f) + curr.velocity * time + curr.position;
        // v = a * t + v_0
        curr.velocity = accel * time + curr.velocity;

        positions[i] = curr.position;
    }
}

void OpenMpSimulator::update(float time) {
    if (collisionsAllowed) {
        handleCollisions();
    }
    calcAccelerations();
    applyAccelerations(time);
}
