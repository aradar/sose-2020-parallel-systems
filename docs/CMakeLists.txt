set(DOXYGEN_HAVE_DOT YES)
set(DOXYGEN_EXTRACT_ALL YES)
set(DOXYGEN_EXTRACT_PRIVATE YES)
set(DOXYGEN_EXTRACT_STATIC YES)
set(DOXYGEN_EXTRACT_PRIV_VIRTUAL YES)
set(DOXYGEN_EXTRACT_PACKAGE YES)
set(DOXYGEN_EXTRACT_LOCAL_METHODS YES)

set(DOXYGEN_CLASS_DIAGRAMS YES)
set(DOXYGEN_CLASS_GRAPH YES)
set(DOXYGEN_COLLABORATION_GRAPH YES)
set(DOXYGEN_CALLER_GRAPH YES)
set(DOXYGEN_CALL_GRAPH YES)
set(DOXYGEN_UML_LOOK YES)
set(DOXYGEN_GROUP_GRAPHS YES)

set(DOXYGEN_TEMPLATE_RELATIONS YES)
set(DOXYGEN_HIDE_UNDOC_RELATIONS NO)
set(DOXYGEN_DOT_TRANSPARENT YES)
set(DOXYGEN_BUILTIN_STL_SUPPORT YES)
set(DOXYGEN_DOT_MULTI_TARGETS YES)
#set(DOXYGEN_IMAGE_FORMAT "svg")
#set(DOXYGEN_INTERACTIVE_SVG YES)

doxygen_add_docs(nbs_docs
    apps/nbs.cpp
    apps/SimulatorApp.hpp
    apps/CliParser.hpp
    include/nbs/Simulator.hpp
    include/nbs/SequentialSimulator.hpp
    include/nbs/OpenMpSimulator.hpp
    include/nbs/OpenClSimulator.hpp
    include/nbs/Visualizer.hpp
    include/nbs/cli/CliParser.hpp
    include/nbs/cli/SimulatorApp.hpp
    src/SequentialSimulator.cpp
    src/OpenMpSimulator.cpp
    src/OpenClSimulator.cpp
    src/Visualizer.cpp
    "${CMAKE_CURRENT_SOURCE_DIR}/mainpage.md"
    WORKING_DIRECTORY "${PROJECT_SOURCE_DIR}")

install(DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/html DESTINATION share/doc)
